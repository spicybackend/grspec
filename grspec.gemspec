# frozen_string_literal: true

require 'rake'

Gem::Specification.new do |spec|
  spec.name        = 'grspec'
  spec.version     = '0.5.0'
  spec.executables << 'grspec'
  spec.summary     = "A simple spec runner for diff'd files"
  spec.description = 'GRSpec is a tool for running specs against files that have changes in source control for quick regression checking.'
  spec.authors     = ['Jordane Lew']
  spec.email       = 'jordane.lew@gmail.com'
  spec.files       = FileList[
                       'bin/grspec',
                       'lib/**/*'
                     ]
  spec.homepage    = 'https://rubygems.org/gems/grspec'
  spec.license     = 'MIT'
  spec.required_ruby_version = '>= 3.0.0'
  spec.metadata = {
    'source_code_uri' => "https://gitlab.com/spicybackend/grspec",
    'changelog_uri' => "https://gitlab.com/spicybackend/grspec/blob/main/CHANGELOG.md",
    'bug_tracker_uri' => "https://gitlab.com/spicybackend/grspec/issues",
  }
end
